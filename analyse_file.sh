#!/bin/sh -e

partial_dump_clean() {
	[ -z "$dumping" ] || rm -f $dumping
}
trap partial_dump_clean EXIT

dir=$1
shift

for file in $@
do
	file $file | grep -q ELF || continue
	dump=$dir/$(basename $file).objdump
	prefix=${file%%/usr/*}
	[ -e $dump ] || {
		dumping=$dump
		mkdir -p $(dirname $dumping)
		objdump -d $file > $dumping 2> /dev/null
		dumping=
	}
	if [ -n "$prefix" ]
	then
		debuginfo_path="--debuginfo-path=$prefix/usr/lib/debug"
	fi
	awk -F : '/v.gather|clwb/ { printf("'${file/$prefix}', " $1 ": "); system("eu-addr2line '$debuginfo_path' -e '$file' -f -i " $1 " 2>/dev/null | tail -2 | head -1"); }' $dump
done
